// Follow this setup guide to integrate the Deno language server with your editor:
// https://deno.land/manual/getting_started/setup_your_environment
// This enables autocomplete, go to definition, etc.

import { serve } from "https://deno.land/std@0.131.0/http/server.ts"
import { LastFM } from "./models/lastfm.ts";

serve(async (req) => {
  // const { name } = await req.json()'
  const user = '';
  const apiKey = '';
  let resp = await fetch(
    `http://ws.audioscrobbler.com/2.0/?method=user.getrecenttracks
      &user=${user}
      &api_key=${apiKey}
      &format=json
      &nowplaying=true`
    );
  const lastFM = await resp.json() as LastFM;
  const track = lastFM?.recenttracks?.track[0];
  const {name, artist: {text}} = track;
  const result = `${name} by ${text}`;
  // console.log(resp.status); // 200
  // console.log(resp.headers.get("Content-Type")); // "text/html"
  // console.log(result); // "Hello, World!"
  
  return new Response(
    JSON.stringify(result),
    { headers: { "Content-Type": "application/json" } },
  )
});
