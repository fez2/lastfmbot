export interface LastFM {
    recenttracks?: Recenttracks;
}

export interface Recenttracks {
    track?: Track[];
    attr?:  Attr;
}

export interface Attr {
    user?:       string;
    totalPages?: string;
    page?:       string;
    perPage?:    string;
    total?:      string;
}

export interface Track {
    artist?:     Album;
    streamable?: string;
    image?:      Image[];
    mbid?:       string;
    album?:      Album;
    name?:       string;
    url?:        string;
    date?:       DateClass;
}

export interface Album {
    mbid?: string;
    text?: string;
}

export interface DateClass {
    uts?:  string;
    text?: string;
}

export interface Image {
    size?: Size;
    text?: string;
}

export enum Size {
    Extralarge = "extralarge",
    Large = "large",
    Medium = "medium",
    Small = "small",
}
